import { connect } from 'react-redux'
import GalleryListComponent from '../components/GalleryListComponent'
import { getGalleryListRequest } from '../GalleryActions'

const mapStateToProps = (state) => ({
  galleryList: state.gallery.galleryList
})

const mapDispatchToProps = (dispatch) => ({
  getGalleryList: (page) => {
    dispatch(getGalleryListRequest(page))
  }
})
export default connect(mapStateToProps, mapDispatchToProps)(GalleryListComponent)
